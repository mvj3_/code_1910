//题目网址: 4629
//http://soj.me/4629

//题目分析:
//先算出分子，分母，再算出分子分母的最大公约数，约分按格式输出即可。

#include <iostream>

using namespace std;

int gcd(int m, int n)
{
    while (n != 0) {
        int rem = m % n;
        m = n;
        n = rem;
    }
    return m;
}

int main()
{
    int T;
    cin >> T;
    while (T--) {
        int a, b, c, d;
        int son, mum;
        cin >> a >> b >> c >> d;
        son = a * d + c * b;
        mum = b * d;
        int tmp = gcd(mum, son);
        if (mum/tmp != 1) {
            cout << son/tmp << '/' << mum/tmp << endl;
        } else {
            cout << son/tmp << endl;
        }
    }
    
    return 0;
}                                 